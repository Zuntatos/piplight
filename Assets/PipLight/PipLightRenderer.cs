#if UNITY_EDITOR
#define PIPLIGHT_EDITOR_SUPPORT
#endif

using UnityEngine;
using UnityEngine.Rendering;
using System.Collections.Generic;
using System;

#if PIPLIGHT_EDITOR_SUPPORT
using UnityEditor;
#endif

public class PipLightSystem
{
	static PipLightSystem instance;

	public static PipLightSystem Instance {
		get {
			if (instance == null)
				instance = new PipLightSystem ();
			return instance;
		}
	}
	
	int _lightsCount = 0;
	public int lightsCount {
		get { return _lightsCount; }
	}
	int lightsIncrement = 16;
	public PipLight[] lights = new PipLight[16];

	public void Add (PipLight o)
	{
		if (_lightsCount >= lights.Length) {
			// Increase the array if it is not large enough
			Array.Resize(ref lights, lights.Length + lightsIncrement);
		}
		// Add to the array and increase its length
		lights[_lightsCount++] = o;
	}

	public void Remove (PipLight o)
	{
		int l = Array.IndexOf(lights, o);
		if (l != -1) {
			// Replace the removed element with the last element on the array. Then remove the original
			lights[l] = lights[--_lightsCount];
			lights[_lightsCount] = null;
		}
	}

	public void RefreshAll ()
	{
		Camera camera = Camera.main;
		for (int i = 0; i < lightsCount; i++) {
			PipLight pipLight = lights[i];
			pipLight.UpdateNextFrame = true;
			pipLight.UpdateLOD(camera);
			pipLight.UpdateShadowMap();
			lights[i].BeforeRender();
		}
	}
}

[ExecuteInEditMode]
public class PipLightRenderer : MonoBehaviour
{
	public Mesh lightSphereMesh;
	Camera lightRendererCamera;

	static Dictionary<Camera, CommandBuffer> buffers = new Dictionary<Camera, CommandBuffer> ();
	static CameraEvent InsertionPoint = CameraEvent.BeforeImageEffectsOpaque;

	[SerializeField]
	void OnDisable ()
	{
		CheckSupport ();
		CommandBuffer usedBuffer;
		if (buffers.TryGetValue (lightRendererCamera, out usedBuffer)) {
			lightRendererCamera.RemoveCommandBuffer (InsertionPoint, usedBuffer);
			usedBuffer.Dispose ();
			buffers.Remove (lightRendererCamera);
		}

		#if PIPLIGHT_EDITOR_SUPPORT
		foreach (SceneView view in SceneView.sceneViews) {
			if (view.camera != null) {
				if (buffers.TryGetValue (view.camera, out usedBuffer)) {
					view.camera.RemoveCommandBuffer (InsertionPoint, usedBuffer);
					usedBuffer.Dispose ();
					buffers.Remove (view.camera);
				}
			}
		}
		#endif
	}

	[SerializeField]
	void OnDestroy ()
	{
		CheckSupport ();
		CommandBuffer usedBuffer;
		if (buffers.TryGetValue (lightRendererCamera, out usedBuffer)) {
			lightRendererCamera.RemoveCommandBuffer (InsertionPoint, usedBuffer);
			usedBuffer.Dispose ();
			buffers.Remove (lightRendererCamera);
		}

		#if PIPLIGHT_EDITOR_SUPPORT
		foreach (SceneView view in SceneView.sceneViews) {
			if (view.camera != null) {
				if (buffers.TryGetValue (view.camera, out usedBuffer)) {
					view.camera.RemoveCommandBuffer (InsertionPoint, usedBuffer);
					usedBuffer.Dispose ();
					buffers.Remove (view.camera);
				}
			}
		}
		#endif
	}

	[SerializeField]
	void OnEnable ()
	{
		lightRendererCamera = GetComponent<Camera> ();
		CheckSupport ();
		CommandBuffer existingBuffer;
		if (buffers.TryGetValue (lightRendererCamera, out existingBuffer)) {
			lightRendererCamera.AddCommandBuffer (InsertionPoint, existingBuffer);
		}

		#if PIPLIGHT_EDITOR_SUPPORT
		foreach (SceneView view in SceneView.sceneViews) {
			if (view.camera != null) {
				if (buffers.TryGetValue (view.camera, out existingBuffer)) {
					view.camera.AddCommandBuffer (InsertionPoint, existingBuffer);
				}
			}
		}
		#endif

		PipLight.CheckKeywords ();
	}

	[SerializeField]
	void LateUpdate ()
	{
		PipLightSystem pipLightSystem = PipLightSystem.Instance;
		PipLight[] lights = pipLightSystem.lights;
		for (int i = 0; i < pipLightSystem.lightsCount; i++) {
			lights[i].BeforeRender ();
		}
		if (lightRendererCamera != null) {
			ReconstructLightBuffers (lightRendererCamera, true);
		}

		#if PIPLIGHT_EDITOR_SUPPORT
		foreach (SceneView view in SceneView.sceneViews) {
			if (view.camera != null) {
				ReconstructLightBuffers (view.camera, false);
			}
		}
		#endif
	}

	public void ForceRefresh ()
	{
		LateUpdate ();
	}

	Plane[] frustrumPlanes = new Plane[6];
	void ReconstructLightBuffers (Camera renderCam, bool toCull)
	{
		if (renderCam == null) {
			return;
		}
		CommandBuffer cameraBuffer;
		if (buffers.TryGetValue (renderCam, out cameraBuffer)) {
			cameraBuffer.Clear ();
		} else {
			cameraBuffer = new CommandBuffer ();
			cameraBuffer.name = "Deferred pipLights";
			renderCam.AddCommandBuffer (InsertionPoint, cameraBuffer);
			buffers.Add (renderCam, cameraBuffer);
		}

		PipLightSystem lightTracker = PipLightSystem.Instance;
		Bounds bounds = new Bounds ();

		if (toCull) {
			GeometryUtilityUser.CalculateFrustumPlanes (frustrumPlanes, renderCam);
		}
		for (int i = 0; i < PipLightSystem.Instance.lightsCount; i++) {
			PipLight pipLight = PipLightSystem.Instance.lights[i];
			if (toCull) {
				bounds.center = pipLight.transform.position;
				bounds.extents = Vector3.one * pipLight.radius;
				if (!GeometryUtility.TestPlanesAABB (frustrumPlanes, bounds)) {
					continue;
				}
			}
			pipLight.UpdateLOD (renderCam);
			pipLight.UpdateShadowMap ();
			pipLight.WriteToCommandBuffer(cameraBuffer, lightSphereMesh, Materials.GetMaterial(pipLight));
		}
	}

	void CheckSupport ()
	{
		if (!lightRendererCamera.hdr) {
			Debug.LogWarning ("PipLights will only work with HDR (non-HDR not implemented", this);
		}
		if (lightRendererCamera.actualRenderingPath != RenderingPath.DeferredShading) {
			Debug.LogWarning ("PipLights will only work with DeferredShading (by design limitations)", this);
		}
	}

	static class Materials
	{
		static Material PointNoShadows;
		static Material PointHardShadows;
		static Material PointSoftShadows;
		static Material PointCookieNoShadows;
		static Material PointCookieHardShadows;
		static Material PointCookieSoftShadows;

		public static Material GetMaterial (PipLight pipLight)
		{
			if (PointNoShadows == null) {
				Initialize ();
			}
			if (pipLight.shadowType == LightShadows.None) {
				return pipLight.cookie == null ? PointNoShadows : PointCookieNoShadows;
			} else if (pipLight.shadowType == LightShadows.Hard) {
				return pipLight.cookie == null ? PointHardShadows : PointCookieHardShadows;
			} else {
				return pipLight.cookie == null ? PointSoftShadows : PointCookieSoftShadows;
			}
		}

		static void Initialize ()
		{
			Shader pipLightShader = Resources.Load<Shader> ("PipLightLight");
			if (pipLightShader == null) {
				Debug.LogError ("PipLightShader not found, is null");
			}
			if (!pipLightShader.isSupported) {
				Debug.LogError ("PipLightShader not supported", pipLightShader);
			}

			PointNoShadows = new Material (pipLightShader);
			PointNoShadows.EnableKeyword ("POINT");
			PointHardShadows = new Material (pipLightShader);
			PointHardShadows.EnableKeyword ("POINT");
			PointHardShadows.EnableKeyword ("SHADOWS_CUBE");
			PointSoftShadows = new Material (pipLightShader);
			PointSoftShadows.EnableKeyword ("POINT");
			PointSoftShadows.EnableKeyword ("SHADOWS_CUBE");
			PointSoftShadows.EnableKeyword ("SHADOWS_SOFT");

			PointCookieNoShadows = new Material (pipLightShader);
			PointCookieNoShadows.EnableKeyword ("POINT_COOKIE");
			PointCookieHardShadows = new Material (pipLightShader);
			PointCookieHardShadows.EnableKeyword ("POINT_COOKIE");
			PointCookieHardShadows.EnableKeyword ("SHADOWS_CUBE");
			PointCookieSoftShadows = new Material (pipLightShader);
			PointCookieSoftShadows.EnableKeyword ("POINT_COOKIE");
			PointCookieSoftShadows.EnableKeyword ("SHADOWS_CUBE");
			PointCookieSoftShadows.EnableKeyword ("SHADOWS_SOFT");
		}
	}

	static class GeometryUtilityUser {
		// http://www.cnblogs.com/bodong/p/4800018.html
		// worldToProjectionMatrix = camera.projectionMatrix * camera.worldToCameraMatrix
		// Planes: 0 = Left, 1 = Right, 2 = Down, 3 = Up, 4 = Near, 5 = Far
		public static void CalculateFrustumPlanes(Plane[] OutPlanes, Camera camera) {
			Matrix4x4 worldToProjectionMatrix  = camera.projectionMatrix * camera.worldToCameraMatrix;

			float ComVector0, ComVector1, ComVector2, ComVector3;
			float RootVector0, RootVector1, RootVector2, RootVector3;

			RootVector0 = worldToProjectionMatrix.m30;
			RootVector1 = worldToProjectionMatrix.m31;
			RootVector2 = worldToProjectionMatrix.m32;
			RootVector3 = worldToProjectionMatrix.m33;

			ComVector0 = worldToProjectionMatrix.m00;
			ComVector1 = worldToProjectionMatrix.m01;
			ComVector2 = worldToProjectionMatrix.m02;
			ComVector3 = worldToProjectionMatrix.m03;

			CalcPlane(ref OutPlanes[0], ComVector0 + RootVector0, ComVector1 + RootVector1, ComVector2 + RootVector2, ComVector3 + RootVector3);
			CalcPlane(ref OutPlanes[1], -ComVector0 + RootVector0, -ComVector1 + RootVector1, -ComVector2 + RootVector2, -ComVector3 + RootVector3);

			ComVector0 = worldToProjectionMatrix.m10;
			ComVector1 = worldToProjectionMatrix.m11;
			ComVector2 = worldToProjectionMatrix.m12;
			ComVector3 = worldToProjectionMatrix.m13;

			CalcPlane(ref OutPlanes[2], ComVector0 + RootVector0, ComVector1 + RootVector1, ComVector2 + RootVector2, ComVector3 + RootVector3);
			CalcPlane(ref OutPlanes[3], -ComVector0 + RootVector0, -ComVector1 + RootVector1, -ComVector2 + RootVector2, -ComVector3 + RootVector3);

			ComVector0 = worldToProjectionMatrix.m20;
			ComVector1 = worldToProjectionMatrix.m21;
			ComVector2 = worldToProjectionMatrix.m22;
			ComVector3 = worldToProjectionMatrix.m23;

			CalcPlane(ref OutPlanes[4], ComVector0 + RootVector0, ComVector1 + RootVector1, ComVector2 + RootVector2, ComVector3 + RootVector3);
			CalcPlane(ref OutPlanes[5], -ComVector0 + RootVector0, -ComVector1 + RootVector1, -ComVector2 + RootVector2, -ComVector3 + RootVector3);
		}

		static void CalcPlane(ref Plane InPlane, float InA, float InB, float InC, float InDistance) {
			Vector3 Normal = new Vector3(InA, InB, InC);

			float InverseMagnitude = 1.0f / Normal.magnitude;
			Normal.x *= InverseMagnitude;
			Normal.y *= InverseMagnitude;
			Normal.z *= InverseMagnitude;

			InPlane.normal = Normal;
			InPlane.distance = InDistance * InverseMagnitude;
		}
	}

}
