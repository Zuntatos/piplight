Shader "Hidden/PipLight/Depth" {
	Properties {
		_MainTex ("", 2D) = "white" {}
		_Cutoff ("", Float) = 0.5
		_Color ("", Color) = (1,1,1,1)
	}
	SubShader{
		Tags{ "RenderType" = "Opaque" }
		Pass{
			Fog { Mode Off }
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			
			float4 Pip_LightPositionRange;
			
			struct v2f {
				float4 pos : SV_POSITION;
				float3 vec : TEXCOORD0;
			};
			
			struct appdata {
				float4 vertex : POSITION;
			};

			v2f vert(appdata v) {
				v2f o;
				o.vec = mul(_Object2World, v.vertex).xyz - Pip_LightPositionRange.xyz; 
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				return o;
			}

			float4 frag(v2f i) : SV_Target {
				return length(i.vec) * Pip_LightPositionRange.w;
			}

			ENDCG
		}
	}
	
	SubShader{
		Tags{ "RenderType" = "TransparentCutout" }
		Pass{
			Fog { Mode Off }
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			
			float4 Pip_LightPositionRange;
			uniform sampler2D _MainTex;
			uniform float4 _MainTex_ST;
			uniform fixed _Cutoff;
			uniform fixed4 _Color;
			
			struct v2f {
				float4 pos : SV_POSITION;
				float3 vec : TEXCOORD0;
				float2 uv : TEXCOORD1;
			};
			
			struct appdata {
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
			};

			v2f vert(appdata v) {
				v2f o;
				o.vec = mul(_Object2World, v.vertex).xyz - Pip_LightPositionRange.xyz; 
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
				return o;
			}

			float4 frag(v2f i) : SV_Target {
				fixed4 texcol = tex2D(_MainTex, i.uv);
				clip(texcol.a * _Color.a - _Cutoff);
				return length(i.vec) * Pip_LightPositionRange.w;
			}

			ENDCG
		}
	}
	Fallback Off
}